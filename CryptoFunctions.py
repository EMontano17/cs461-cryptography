import string
import math
import itertools
from time import time
import random
import fractions


def encrypt_RSA(p, q, e, plaintext):
    n = p * q
    totient = (p - 1) * (q - 1)
    if pgcd(e, totient)[0] != 1:
        return "Fail (GCD(e, totient(n) != 1)"
    d = modular_inverse(e, totient).split()[2]
    ciphertext = big_exponent(plaintext, e, n)
    return "n: {}, totient: {}, d: {}, ciphertext: {}".format(n, totient, d, ciphertext)


def decrypt_RSA(d, n, ciphertext):
    plaintext = big_exponent(ciphertext, d, n)
    return "plaintext: {}".format(plaintext)


# prints time difference between two given times
def difference(start, stop):
    # check for small numbers that will round to 0
    if(stop - start) < 0.01:
        print("It took < 0.01 seconds.")
    # otherwise, print the difference (formatted) to stdout
    else:
        print("It took {:.2f} seconds.".format(stop - start))
    print()


# brute force method for finding factor
def brute_force(n):
    print("Brute Force Factoring")
    start = time()
    # Only need to check the range from 2 to the square root, rounded down
    for i in range(2, math.floor(math.sqrt(n)) + 1):
        # If a factor is found
        if n % i == 0:
            # Print the result
            print("Found a factor = {}".format(i))
            stop = time()
            # Print the time
            difference(start, stop)
            # Break
            return True
    print("Didn\'t find a factor.")
    stop = time()
    difference(start, stop)
    return False


# The function used for pollard_rho
def pr_function(x):
    return x**2 + 1


# Finds the GCD of 2 numbers recursively using the Euclidean Algorithm
def pgcd(fa, fb):
    if fa == 0:
        return fb, 0, 1
    else:
        g, y, x = pgcd(fb % fa, fa)
        return g, x - (fb // fa) * y, y


# pollard rho method for finding factor
def pollard_rho(n):
    print("Pollard Rho")
    counter = 0
    factors = []

    start = time()
    # Only test for a factor a finite number of times
    while len(factors) == 0 and counter < 100000:
        counter += 1

        # Create a and b for checking numbers
        a = random.randint(1, 200)
        b = a
        f = pgcd(abs(a - b), n)[0]
        if 1 < f < n:
            factors.append(f)
        # Use a designated function to advance a and b
        a = pr_function(a) % n
        b = pr_function(pr_function(b) % n) % n

        while a != b:
            f = pgcd(abs(a - b), n)[0]
            # Check for a factor
            if 1 < f < n and f not in factors:
                factors.append(f)

            # Use a designated function to advance a and b
            a = pr_function(a) % n
            b = pr_function(pr_function(b) % n) % n
    stop = time()

    if len(factors) > 0:
        print("Found a factor = {}".format(factors[0]))
    else:
        print("Didn\'t find a factor.")
    difference(start, stop)


# Miller Rabine method for finding prime
def is_prime(n, t=1):
    # use miller to find if n is prime
    if n == 2 or n == 3:
        return True
    if n < 2 or t < 1:
        return False

    # Solve for n - 1, s, and r
    s = 0
    n_minus = n - 1
    while n_minus % 2 == 0:
        s += 1
        n_minus /= 2
    r = (n - 1) // (2**s)

    # For number of tests (will only loop once by default)
    for i in range(1, t + 1):
        # Random witness
        a = random.randint(2, n - 2)
        y = pow(a, r, n)
        if y != 1 and y != (n - 1):
            j = 1
            while j <= (s - 1) and y != (n - 1):
                y = pow(y, 2, n)
                if y == 1:
                    # the number is composite
                    return False
                j += 1
            if y != (n - 1):
                # the number is composite
                return False
    # If the number has passed all tests, it is prime
    return True


def factor_over_base(n, mod, bases, c):
    factors = []
    factored = pow(n, 2, mod)
    statement = (str(c) + " " + str(n) + " === " + str(factored)).ljust(len(str(pow(mod, 2))) * 2 + 5, " ")

    # Check each base to get factorization for number
    for b in bases:
        sum = 0
        # While the current base is a factor
        while factored % b == 0 and factored != 0:
            # Remove that factor from the number
            factored //= b
            factors.append(b)
            sum += 1
        statement += str(sum) + " "
    if factored == 1:
        print(statement)
        return factors
    else:
        return ""


# Dixon's Algorithm for finding a factor
def dixon(n, b):
    print("Dixon\'s Algorithm")
    factors = []
    # Get user input for number of factors in base
    factors_len = b

    # Create list of first primes until list is the required length
    finder = 2
    while len(factors) < int(factors_len):
        if is_prime(finder):
            factors.append(finder)
        finder += 1

    print("Done generating factor base.")

    start = time()

    # Create list of equations
    possibles = []
    counter = 1
    while len(possibles) < int(factors_len):
        test = random.randint(2, n)
        if factor_over_base(test, n, factors, counter) != "":
            possibles.append(test)
            counter += 1
    print()

    # Check for x^2 = y^2 (mod n) for each equation
    for p in possibles:
        y_pos = math.sqrt(pow(p, 2, n))

        if (y_pos * 10) % 10 == 0:
            x = p
            y = int(y_pos)
            f = pgcd(abs(x - y), n)[0]
            if (x % n) != (abs(y) % n) and f != 1 and f != n:
                print("Found a factor = {}".format(f))
                stop = time()
                difference(start, stop)
                return
    if (math.sqrt(n) * 10) % 10 == 0:
        print("Found a factor = {}".format(int(math.sqrt(n))))
        stop = time()
        difference(start, stop)
        return

    # If the last check doesn't yield a result, check combinations of the equations
    # Combine numbers to check
    combined = []
    for i in range(2, len(possibles) + 1):
        # Itertools creates every possible multiplication of the current equations
        for combo in itertools.combinations(possibles, i):
            total = 1
            for num in combo:
                total *= num
            combined.append(total)

    # Check for x^2 = y^2 (mod n) for each combination
    for c in combined:
        y_pos = math.sqrt(pow(c, 2, n))
        if (y_pos * 10) % 10 == 0:
            x = c
            y = int(y_pos)
            f = gcd(abs(x - y), n)[0]
            if (x % n) != (abs(y) % n) and f != 1 and f != n:
                print("Found a factor = {}".format(f))
                stop = time()
                difference(start, stop)
                return

    stop = time()
    print("Didn\'t find a factor.")
    difference(start, stop)


def factor(n, b):
    if brute_force(n):
        pollard_rho(n)
        dixon(n, b)
        return "See output in stdout"
    return "{} is prime.".format(n)


class A:
    def __init__(self):
        self.alphabet = string.ascii_lowercase

    def set_alphabet(self, m):
        self.alphabet = m

    def get_alphabet(self):
        return self.alphabet


ALPHABET = A().get_alphabet()


def notes():
    return "Perfect secrecy is when H(P|C) = H(P).\n" \
           "The joint entropy of two events is maximized when\n" \
           "those events are independent, and can never exceed\n" \
           "the sum of the individual entropies.\n" \
           "Kerckhoff's principle- you cannot rely on security thourgh\n" \
           "obscurity. The means of encryption are not secret.\n" \
           "Joint probability: P(E1, E2) = P(E1) * P(E2) if and only if\n" \
           "the events are independent.\n" \
           "Conditional Probability: P(E1) given that E2 happened.\n" \
           "P(E1 | E2) = P(E1, E2) / P(E2)\n" \
           "Entropy: H(x) = -sum(Pi * log(Pi))\n" \
           "Joint Entropy: H(x, y) = -sum(P(X=x | Y=y) * log(P(X=x | Y=y)))\n" \
           "Conditional Entropy: see notes\n" \
           "Generators = primitive roots\n" \
           "Fermatt\'s Little Theorem- if p is prime and p does not divide a\n" \
           "a^(p - 1) = 1 (mod p)\n" \
           "Basic Principle for factoring- Given a number n to factor,\n" \
           "if we can find 2 numbers x and y such that x^2 = y^2 (mod n)\n" \
           "and x != +/- y (mod n) then GCD(|x - y|, n) is a nontrivial factor of n)\n" \
           "Diffie Helmann is used for key transfer:" \
           "Bob chooses a large prime p and primitive root alpha\n" \
           "Alice has a secure message m such that 0 < m < p\n" \
           "Bob chooses a sercret a; computes beta=alpha^a (mod rho); publishes (rho, alpha, beta)\n" \
           "Alice chooses a secret k; computes r=alpha^k (mod rho); computes t=beta^k*m (mod rho)\n" \
           "Alice sends (r, t) to Bob\n" \
           "Bob decrypts m=t*r^-a (mod rho)"


def encrypt_Diffie(k, a, b, r, m):
    # r = big_exponent(a, k, r)
    # t = (big_exponent(b, k, r) * m) % r
    # return "r: {}, t: {}".format(r, t)
    return ""


def decrypt_Diffie(a, t, r, rho):
    m = (big_exponent(int(modular_inverse(r, rho).split()[2]), a, rho) * t) % rho
    return "m: {}".format(m)


def totient(n):
    amount = 0
    for k in range(1, n + 1):
        if fractions.gcd(n, k) == 1:
            amount += 1
    return amount


def is_primitive_root(base, mod):
    if big_exponent(base, mod - 1, mod) != 1:
        return False
    for i in range(1, mod - 1):
        print(big_exponent(base, i, mod))
        if big_exponent(base, i, mod) == 1:
            return False
    return True


def first_primitive_root(mod):
    x = 1
    while x < mod:
        if is_primitive_root(x, mod):
            return x
        x += 1
    return "There isn\'t a primitive root."



def get_common(a, b):
    result = []
    for thing in a:
        if thing in b:
            result.append(thing)
    return result


def gauss(equations):
    max_num = 1
    for thing in equations:
        max_num *= thing[2]
    possibilities = almost_modular_inverse(equations[0][0], equations[0][1], equations[0][2], max_num)
    for thing in equations:
        current = almost_modular_inverse(thing[0], thing[1], thing[2], max_num)
        possibilities = get_common(possibilities, current)
    return possibilities


def almost_modular_inverse(a, remainder, mod, limit=0):
    if limit == 0:
        limit = mod
    possibilities = []
    for i in range(limit):
        if a * i % mod == remainder:
            possibilities.append(i)
    return possibilities


def almost_modular_inverse_squared(remainder, mod, a=1):
    possibilities = []
    for i in range(mod):
        if a * pow(i, 2) % mod == remainder:
            possibilities.append(i)
    return possibilities


def pretty_results(m, k):
    result = ""
    for thing in probable_key_vignere(m, k):
        result += thing[0] + " in spot " + str(thing[1]) + " gives " + thing[2] + "\n"
    return result


def probable_key_vignere(m, key_len):
    results = []
    for i in range(key_len):
        for letter in ALPHABET:
            current = ""
            for x in range(i, len(m), key_len):
                current += decrypt_vignere(m[x].lower(), letter)
            results.append((letter, i, current))
    return results


def probable_shift_vignere(m, l=0):
    if l > 0:
        limit = l
    else:
        limit = len(m)
    mtest = m
    shift = 0
    highest = 0
    result = -1
    while len(mtest) > 0 and shift < limit:
        mtest = mtest[1:]
        shift += 1
        count = 0
        for i in range(len(mtest)):
            if m[i] == mtest[i]:
                count += 1
        if count > highest:
            highest = count
            result = shift
    return result


def encrypt_caesar(m, s):
    encrypted = ""
    for char in m.lower():
        if char in ALPHABET:
            encrypted += ALPHABET[(ALPHABET.index(char) + s) % len(ALPHABET)]
        else:
            encrypted += char
    return encrypted


def decrypt_caesar(m, s):
    return encrypt_caesar(m, len(ALPHABET) - s)


def all_caesar(m):
    possible = []
    for i in range(len(ALPHABET)):
        possible.append(encrypt_caesar(m, i))
    return possible


def pretty_print_caesar(m):
    result = ""
    counter = 0
    for thing in all_caesar(m):
        result += str(counter) + ": " + thing + "\n"
        counter += 1
    return result


def encrypt_affine(m, a, b):
    encrypted = ""
    for char in m.lower():
        if char in ALPHABET:
            encrypted += ALPHABET[(a * (ALPHABET.index(char)) + b) % len(ALPHABET)]
        else:
            encrypted += char
    return encrypted


def decrypt_affine(m, a, b):
    decrypted = ""
    limit = pow(len(ALPHABET), 2) + 122
    for char in m.lower():
        if char in ALPHABET:
            i = ALPHABET.index(char) - b
            while i < 0:
                i += len(ALPHABET)
            while i % a != 0 and i < limit:
                i += len(ALPHABET)
            i //= a
            decrypted += ALPHABET[i]
        else:
            decrypted += char
    return decrypted


def encrypt_vignere(m, k):
    encrypted = ""
    for i in range(len(m)):
        if m[i].lower() in ALPHABET:
            encrypted += encrypt_caesar(m[i].lower(), ALPHABET.index(k[i % len(k)].lower()))
        else:
            encrypted += m[i]
    return encrypted


def decrypt_vignere(m, k):
    decrypted = ""
    for i in range(len(m)):
        if m[i].lower() in ALPHABET:
            decrypted += decrypt_caesar(m[i], ALPHABET.index(k[i % len(k)].lower()))
        else:
            decrypted += m[i]
    return decrypted


def entropy(p):
    summer = 0
    parts = []
    for thing in p:
        ind = -1 * thing[1] * math.log(thing[1], 2)
        summer += ind
        parts.append((thing[0], ind))
    return summer, parts


def egcd(a, b):
    if a == 0:
        return b, 0, 1
    else:
        g, y, x = egcd(b % a, a)
        return g, x - (b // a) * y, y


def gcd(a, b):
    result = egcd(a, b)
    if result[2] < 0:
        new_result = (result[0], result[1], result[2] + b)
    else:
        new_result = result
    return "gcd: {} x: {} y: {}".format(new_result[0], new_result[1], new_result[2])


def e_modular_inverse(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        return "modular inverse does not exist"
    else:
        return x % m


def modular_inverse(a, m):
    return "multiplicative inverse: {}".format(e_modular_inverse(a, m))


def big_exponent(base, exp, mod):
    answer = 1
    while exp > 0:
        if exp % 2 == 0:
            base = (pow(base, 2)) % mod
            exp /= 2
        else:
            answer = (base * answer) % mod
            exp -= 1
    return answer

# examples
"""
print(decrypt_caesar("bqqmf", 1))
print(encrypt_caesar("apple", 27))
print(all_caesar("bqqmf"))
print(encrypt_vignere("TESTMESSAGE", "balls"))
print(decrypt_vignere("UEDEEFSDLYF", "balls"))
print(entropy([("A", 0.5), ("B", 0.25), ("C", 0.25)]))
print(notes())
print(entropy([("A", 0.2), ("B", 0.3), ("C", 0.5)]))
print(entropy([("HH", 0.25), ("HT", 0.25), ("TH", 0.25), ("TT", 0.25)]))
print(entropy([("r", 6/10), ("g", 2/10), ("b", 2/10)]))
print(entropy([("r", 5/9), ("g", 2/9), ("b", 2/9)]))
print(gcd(19, 103))
print(modular_inverse(60002, 411))
print(modular_inverse(13, 101))
print(modular_inverse(101, 13))
print(big_exponent(2, 12345, 789))
print(probable_shift_vignere("ABCBABBBAC"))
print(probable_shift_vignere("ABCBABBBAC", 3))
print(encrypt_affine("harry", 7, 3))
print(decrypt_affine("adssp", 7, 3))
#print(probable_key_vignere("ABCBABBAC", 2))
print(almost_modular_inverse(6, 11, 21))
print(almost_modular_inverse(15, 6, 27))
print(almost_modular_inverse_squared(11, 35))
print(gauss([(1, 3, 5), (1, 14, 17), (1, 7, 11)]))
#brute force, pollard rho, dixons
print(factor(57, 8))
print(factor(67, 8))
#miller rabine
print(is_prime(67))
print(is_prime(384762387623))
print(encrypt_RSA(17, 19, 5, 209))
print(decrypt_RSA(173, 323, 133))
print(is_primitive_root(2, 7))
print(is_primitive_root(3, 7))
print(is_primitive_root(15, 23))
print(first_primitive_root(71))
print(first_primitive_root(23))
print(totient(300))
print(totient(28))
"""


# number 2
def verbose_primitive_root(base, mod):
    factors = [1]
    t = mod - 1

    for i in range(2, math.floor(math.sqrt(t))+1):
        while t % i == 0 and t != 1:
            # Remove that factor from the number
            t //= i
            factors.append(i)
    print("factors: {}".format(factors))
    print("Now we do (mod - 1)/(factor) and use that as our exponent (mod n)")

    checks = []
    for j in list(set(factors)):
        exp = (mod-1) // j
        print("{}^{} = {} (mod {})".format(base, exp, big_exponent(base, exp, mod), mod))

    print("The first one must be equal to 1 for it to be a primitive root and the following numbers must not equal each other or one.")


# number 3
def last_x_digits(base, exp, x):
    a = pow(10, x)
    r = pow(base, exp, a)
    print("The last {} digits of {}^{} (mod {}) are {}.".format(x, base, exp, a, r))


# number 4
def miller_rabine(n, a=0, t=1):
    # use miller to find if n is prime
    if n == 2 or n == 3:
        print("prime")
    if n < 2 or t < 1:
        print("composite")

    # Solve for n - 1, s, and r
    s = 0
    n_minus = n - 1
    while n_minus % 2 == 0:
        s += 1
        n_minus /= 2
    r = (n - 1) // (2**s)
    print("N: {}, A: {}, S: {}, R: {}".format(n, a, s, r))

    # For number of tests (will only loop once by default)
    for i in range(1, t + 1):
        # Random witness
        if a == 0:
            a = random.randint(2, n - 2)
        y = pow(a, r, n)
        if y != 1 and y != (n - 1):
            j = 0
            print("J: {}, A^((2^j)*R) = {} (mod {})".format(j, y, n))
            j = 1
            while j <= (s - 1) and y != (n - 1):
                y = pow(y, 2, n)
                print("J: {}, A^((2^j)*R) = {} (mod {})".format(j, y, n))
                if y == 1:
                    # the number is composite
                    print("not prime.")
            if y != (n - 1):
                # the number is composite
                print("not prime.")
    # If the number has passed all tests, it is prime
    print("probably prime.")


# number 5
def get_gamma(alpha, N, mod):
    print("Use the excel sheet as an aid to this problem.")
    print(big_exponent(e_modular_inverse(alpha, mod), N, mod))


def get_x(j, k, N):
    print("Use the excel sheet as an aid to this problem.")
    print(j + N * k)


def get_pandq(start):
    found = []
    tester = start + 1

    while len(found) < 2:
        if is_prime(tester):
            found.append(tester)
        tester += 1

    print("p: {}, q: {}".format(found[0], found[1]))


# number 6
def get_nandtotient(p, q):
    n = p * q
    totient = (p - 1) * (q - 1)
    print("n: {}, totient: {}".format(n, totient))


# number 7
def rsa_stuff(n, p, q, totient, M):
    found = False
    e = 0

    while not found:
        # e = random.randint(200, 1000)
        e = 251
        if pgcd(e, totient)[0] == 1:
            found = True
    d = int(modular_inverse(e, totient).split()[2])
    print("e: {}, d: {}, public key: (n: {}, e: {}), private key: (p: {}, q: {}, d: {})".format(e, d, n, e, p, q, d))
    C = pow(M, e, n)
    print("C = M^e (mod n) so {} = {}^{} (mod {}) {} is the ciphertext".format(C, M, e, n, C))
    print("M = C^d (mod n) so {} = {}^{} (mod {}) {} is the checked plaintext".format(pow(C, d, n), C, d, n, pow(C, d, n)))


# number 8
def factors_of(n, base):
    test = n
    factors = []
    for b in base:
        while test % b == 0 and b != 1:
            test //= b
            factors.append(b)
    return factors


def multiply(factors):
    product = 1
    result = ""
    for factor in factors:
        product *= factor
        result += str(factor) + " * "
    print(result[:-2] + " = {}".format(product))
    return product


def dixons(n, b=0):
    if b == 0:
        b = math.floor(math.sqrt(n))

    factors = []
    # Get user input for number of factors in base
    factors_len = b

    # Create list of first primes until list is the required length
    finder = 2
    while len(factors) < int(factors_len):
        if is_prime(finder):
            factors.append(finder)
        finder += 1

    print("Done generating factor base.")

    # Create list of equations
    possibles = []
    counter = 1
    while len(possibles) < int(factors_len):
        test = random.randint(2, n)
        if factor_over_base(test, n, factors, counter) != "":
            possibles.append(test)
            counter += 1
    print()

    # Check for x^2 = y^2 (mod n) for each equation
    for p in possibles:
        y_pos = math.sqrt(pow(p, 2, n))

        if (y_pos * 10) % 10 == 0:
            x = p
            y = int(y_pos)
            f = pgcd(abs(x - y), n)[0]
            if (x % n) != (abs(y) % n) and f != 1 and f != n:
                print("Found a factor = {} and therefore also {}".format(f, n // f))
                a = pow(x, 2, n)
                b = factors_of(a, factors)
                c = pow(multiply(factors_of(a, factors)), 2, n)
                print("x: {}, x^2 (mod n): {}, factors: {}".format(x, a, b))
                print("({})^2 (mod {}) = {}".format(b, n, c))
                print("GCD(|{} - {}|, {}) = {}".format(a, c, n, f))
                return
    if (math.sqrt(n) * 10) % 10 == 0:
        print("Found a factor = {}, which was the square root".format(int(math.sqrt(n))))
        return

    # If the last check doesn't yield a result, check combinations of the equations
    # Combine numbers to check
    combined = []
    for i in range(2, len(possibles) + 1):
        # Itertools creates every possible multiplication of the current equations
        for combo in itertools.combinations(possibles, i):
            total = 1
            for num in combo:
                total *= num
            combined.append(total)

    # Check for x^2 = y^2 (mod n) for each combination
    for c in combined:
        y_pos = math.sqrt(pow(c, 2, n))
        if (y_pos * 10) % 10 == 0:
            x = c
            y = int(y_pos)
            f = gcd(abs(x - y), n)[0]
            if (x % n) != (abs(y) % n) and f != 1 and f != n:
                print("Found a factor = {} and therefore also {}".format(f, n // f))
                return
'''
verbose_primitive_root(3, 17)
last_x_digits(11, 293, 2)
miller_rabine(181, 6)
get_gamma(2, 8, 59)
get_x(7, 1, 8)
get_pandq(128)  # 128 is start of 8 bit primes
get_nandtotient(131, 139)
rsa_stuff(437, 19, 23, 396, 103)
dixons(517)
'''
