import time
import math
import random

def gcd(a, b):
    while b != 0:
        c = a % b
        a = b
        b = c
    return a

def is_prime(n):
    for i in range(3, n):
        if n % i == 0:
            return False
    return True

def brute_factor(trial):
    start = time.time()
    factor = 0
    for i in range(1,int(math.sqrt(trial)+1)):
        if trial % i == 0:
            if is_prime(i):
                factor = i
    return factor, (time.time()-start)

def pollard_rho(trial):
    start = time.time()
    t = int(random.random()*trial)
    h = int(random.random()*trial)
    factor = 1
    while(factor == 1):
        t = (t**2+1) % trial
        h = (h*2 + 1) % trial
        test_factor = gcd(abs((t-h)), trial)
        if is_prime(test_factor):
            factor = test_factor
    return factor, (time.time()-start)

def prime(n):
        def isPrime(n):
            return not [x for x in xrange(2,int(math.sqrt(n))+1)
                        if n%x == 0]
        primes = []
        candidates = xrange(2,n+1)
        candidate = 2
        while not primes and candidate in candidates:
            if n%candidate == 0 and isPrime(candidate):
                primes = primes + [candidate] + prime(n/candidate)
            candidate += 1
        return primes

def dixons_part_1(trial, base):

    base_num = len(base)
    start_b = int(math.sqrt(trial))

    num_tested_save = []
    tested = []
    while(len(num_tested_save) < base_num):
        rand = int(random.random()*(math.sqrt(trial)))
        while(rand in tested):
            rand = int(random.random()*(math.sqrt(trial)))
            while (rand < 1000):
                rand = int(random.random()*(math.sqrt(trial)))
                while (rand == 0 and rand == 1):
                    rand = int(random.random()*(math.sqrt(trial)))
                    while isPrime(rand):
                        rand = int(random.random()*(math.sqrt(trial)))
        #print "testing:", rand
        tested.append(rand)
        #print "tested:", tested

        list_o_primes = prime(rand)
        dict_primes = {x:list_o_primes.count(x) for x in list_o_primes}
        #print "primes:", dict_primes
        keys, vals = dict_primes.keys(), dict_primes.values()

        len_test = 0
        for i in keys:
            if i not in base:
                break
            len_test += 1

        if len_test == len(keys):
            num_tested_save.append(rand)
    return num_tested_save


def dixons(trial, base_num):
    base = []
    base_check = 2
    while(len(base) != base_num):
        if isPrime(base_check):
            base.append(base_check)
        base_check += 1

    print "base:", base
    start = time.time()

    num_pairs = 0
    factor = 1
    while(num_pairs == 0 and factor == 1):
        num_tested_save = dixons_part_1(trial,base)

        print "testing:", num_tested_save

        x = 0
        y = 0
        for i in num_tested_save:
            for j in num_tested_save:
                lhs = (i*j)**2
                rhs = lhs%trial
                if (math.sqrt(rhs)) == 0 and (i != j):
                        print i,j,"===", rhs
                        num_pairs += 1
                        x = (i*j)
                        y = int(math.sqrt(rhs))
                        break
            if num_pairs == 1:
                break

        factor = prime(gcd(x-y,trial))

    return factor[0], (time.time()-start)

def main(num):
    print "\nBrute force factoring"
    brute, time_b = brute_factor(num)
    print "Found a factor =", brute
    print "It to %0.8f seconds" % time_b

    print "\nPollard Rho"
    pollard, time_p = pollard_rho(num)
    print "Found a factor =", pollard
    print "It to %0.8f seconds" % time_p

    print "\nDixon's Algorithm"
    base = input("Enter # of factors in factor base: ")
    print "Done generating factor base."
    pollard, time_p = dixons(num, base)
    print "Found a factor =", pollard
    print "It to %0.8f seconds" % time_p


if __name__ == "__main__":
    print "PEX1 - Factoring! - by C1C Montano"
    print "CS431\n"
    num_to_factor = input("Enter a number to factor: ")
    main(num_to_factor)