import random, math

alpha = "abcdefghijklmnopqrstuvwxyz"
alpha_cap = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

#    _   _                                           ______                _   _
#   | \ | |                                         |  ____|              | | (_)
#   |  \| | ___  ___ ___  ___ ___  __ _ _ __ _   _  | |__ _   _ _ __   ___| |_ _  ___  _ __  ___
#   | . ` |/ _ \/ __/ _ \/ __/ __|/ _` | '__| | | | |  __| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
#   | |\  |  __/ (_|  __/\__ \__ \ (_| | |  | |_| | | |  | |_| | | | | (__| |_| | (_) | | | \__ \
#   |_| \_|\___|\___\___||___/___/\__,_|_|   \__, | |_|   \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
#                                             __/ |
#                                            |___/
def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def isPrime(n):
    return not [x for x in xrange(2,int(math.sqrt(n))+1)
                if n%x == 0]

def prime(n):
    primes = []
    candidates = xrange(2,n+1)
    candidate = 2
    while not primes and candidate in candidates:
        if n%candidate == 0 and isPrime(candidate):
            primes = primes + [candidate] + prime(n/candidate)
        candidate += 1
    return primes

def phi(n):
    y = n
    for i in range(2,n+1):
        if isPrime(i) and n % i == 0:
            y *= 1 - 1.0/i
    return int(y)

def gcd(a, b):
    while b != 0:
        c = a % b
        a = b
        b = c
    return a

#     _____                  _          ______                _   _
#    / ____|                | |        |  ____|              | | (_)
#   | |     _ __ _   _ _ __ | |_ ___   | |__ _   _ _ __   ___| |_ _  ___  _ __  ___
#   | |    | '__| | | | '_ \| __/ _ \  |  __| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
#   | |____| |  | |_| | |_) | || (_) | | |  | |_| | | | | (__| |_| | (_) | | | \__ \
#    \_____|_|   \__, | .__/ \__\___/  |_|   \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
#                 __/ | |
#                |___/|_|
"""
-*- Affine -*-
//inputs
a => alpha
b => beta
msg => message to encrypt or decrypt

//outputs
encrypted or decrypted message
"""
def affine_encrypt(a,b, msg):
    encrypted = ""
    for x in msg:
        print x
        if x.isalpha() == False:
            pass
        else:
            y = (a*alpha.index(x)+b) % len(alpha)
            encrypted += alpha_cap[y]
    return encrypted

"""
-*- Affine -*-
//inputs
a => alpha
b => beta
msg => message to encrypt or decrypt

//outputs
encrypted or decrypted message
"""
def affine_decrypt(a, b, msg):
    decrypted = ""

    for y in msg:
        print y
        if y.isalpha() == False:
            pass
        else:
            x = mod_inv(a, len(alpha_cap))*(alpha_cap.index(y)-b) % len(alpha_cap)
            decrypted += alpha[x]
    return decrypted


"""
-*- shift -*-
//inputs
shift => number of characters to shift by
msg => message to encrypt or decrypt

//outputs
encrypted or decrypted message
"""
def shift_encrypt(shift, msg):
    encrypted = ""
    for x in msg:
        if x.isalpha() == False:
            pass
        else:
            y = (alpha.index(x)+shift) % len(alpha)
            encrypted += alpha_cap[y]
    return encrypted

"""
-*- shift -*-
//inputs
shift => number of characters to shift by
msg => message to encrypt or decrypt

//outputs
encrypted or decrypted message
"""
def shift_decrypt(shift, msg):
    decrypted = ""
    for x in msg:
        if x.isalpha() == False:
            pass
        else:
            y = (alpha_cap.index(x)+ (len(alpha)-shift)) % len(alpha)
            decrypted += alpha[y]
    return decrypted


"""
-*- Modular Inverse -*-
//inputs
a => the number you are comparing
m => the mod

//outputs
x => the answers that are possible
"""
def mod_inv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m

"""
-*- Modular Math --> ax=z (mod b) -*-
//inputs
a => the number being multiplied
b => the number that is used for the mod
z => the number that you are searching for

//outputs
x => the answers that are possible
"""
def mod_list(a,b,z=-1):
    if z == -1:
        for x in range(1,b):
            mod = (a*x)%b
            print '(',a,'*',x,") %",b,'=',mod
    else:
        for x in range(1,b):
            mod = (a*x)%b
            if mod == z:
                print '(',a,'*',x,") %",b,'=',mod
            elif x == b-1:
                print "There is no value that satisfies: ", '('+str(a),"* x) %",b,'=',z

"""
-*- Modular Math --> x*x=z (mod b) -*-
//inputs
b => the number that is used for the mod
z => the number that you are searching for

//outputs
x => the answers that are possible.
"""
def mod_sq(b,z=-1):
    if z == -1:
        for x in range(1,b**2):
            mod = (x*x)%b
            print '(',x,'*',x,") %",b,'=',mod
    else:
        for x in range(1,b**2):
            mod = (x*x)%b
            if mod == z:
                print '(',x,'*',x,") %",b,'=',mod
            elif x == b-1:
                print "There is no value that satisfies: ", "( x * x ) %",b,'=',z

"""
-*- Fermat's Checker -*-
//inputs
a => start number (non-zero)
z => end number

//outputs
x => the answers that are possible.
"""
def fermat_checker(a,z):

    def fermat(n):
        if n == 2:
            return True
        if not n & 1:
            return False
        return pow(2, n-1, n) == 1

    for i in range(a,z):
        string = str(i)
        if fermat(i) and isPrime(i):
            string += " Prime & Fermat Pseudoprime"
        elif fermat(i) and isPrime(i) == False:
            string += " ***** NOT PRIME but Fermat Pseudoprime *****"
        print string


"""
-*- miller raban -*-
//inputs
number => number to be checked
q => number of trials

//outputs
Reuturns prime or composite
"""
def miller_raban(n,k):
    assert n >= 2
    # special case 2
    if n == 2:
        return True
    # ensure n is odd
    if n % 2 == 0:
        return False
    # write n-1 as 2**s * d
    # repeatedly try to divide n-1 by 2
    s = 0
    d = n-1
    while True:
        quotient, remainder = divmod(d, 2)
        if remainder == 1:
            break
        s += 1
        d = quotient
    assert(2**s * d == n-1)

    # test the base a to see whether it is a witness for the compositeness of n
    def try_composite(a):
        if pow(a, d, n) == 1:
            return False
        for i in range(s):
            if pow(a, 2**i * d, n) == n-1:
                return False
        return True # n is definitely composite

    for i in range(k):
        a = random.randrange(2, n)
        if try_composite(a):
            print i, "-->", a, "False"
            return False
        print i, "-->", a, "True"
    return True

"""
-*- two factoring -*-
//inputs
x => number to be factored

//outputs
Returns prime factoring with twos
"""
def two_factoring(x):
    primes = prime(x)
    num_twos = 0
    odd = 1
    for i in primes:
        if i == 2:
            num_twos += 1
        else:
            odd = odd * i

    print "The factoring is: 2^"+str(num_twos)+"*"+str(odd)
    print "The factors are:", primes

"""
-*- Primitive Root -*-
//inputs
n => number to be rooted
m => the modulus

//outputs
Returns primitive roots
"""
def primitive_root(n,m):
    p = phi(m)
    lcheck = []
    for i in range(1,p+1):
        check = pow(n,i,m)
        if check != 1:
            if check not in lcheck:
                lcheck.append(check)
            else:
                print lcheck
                return False
        else:
            lcheck.append(check)
            if len(lcheck) == p:
                print lcheck
                return True
            else:
                print lcheck
                return False

"""
-*- Last Digits -*-
//inputs
n => number
p => power
d => num digits (default 2)

//outputs
Returns the last number digits
"""
def last_digits(n,p,d):
    digits = str(pow(n,p))
    return digits[(len(digits)-d):]

def dixons_part_1(trial, base, rand):

    base_num = len(base)
    start_b = int(math.sqrt(trial))

    num_tested_save = []
    tested = []
    while(len(num_tested_save) < base_num):

        #print "testing:", rand
        tested.append(rand)
        #print "tested:", tested

        list_o_primes = prime(rand)
        dict_primes = {x:list_o_primes.count(x) for x in list_o_primes}
        #print "primes:", dict_primes
        keys, vals = dict_primes.keys(), dict_primes.values()

        len_test = 0
        for i in keys:
            if i not in base:
                break
            len_test += 1

        if len_test == len(keys):
            num_tested_save.append(rand)

        rand += 1
    return num_tested_save


def dixons(trial, base_num, start):
    base = []
    base_check = 2
    while(len(base) != base_num):
        if isPrime(base_check):
            base.append(base_check)
        base_check += 1

    num_pairs = 0
    factor = 1
    while(num_pairs == 0 and factor == 1):
        num_tested_save = dixons_part_1(trial,base,start)

        x = 0
        y = 0
        for i in num_tested_save:
            for j in num_tested_save:
                lhs = (i*j)**2
                rhs = lhs%trial
                if (math.sqrt(rhs))%1 == 0 and (i != j):
                        num_pairs += 1
                        x = (i*j)
                        y = int(math.sqrt(rhs))
                        print "("+str(i)+"x"+str(j)+")","===",lhs,"===",rhs, "("+str(y)+"x"+str(y)+")"
        factors=[]
        plus = gcd(x+y,trial)
        minus = gcd(x-y,trial)
        factors.append(plus)
        factors.append(minus)
        print "gcd("+str(x)+"-"+str(y)+","+str(trial)+") =", minus
        print "gcd("+str(x)+"+"+str(y)+","+str(trial)+") =", plus

    return factors

def pollard_rho(trial):
    t = int(random.random()*trial)
    h = int(random.random()*trial)
    factor = 1
    while(factor == 1):
        t = (t**2+1) % trial
        h = (h*2 + 1) % trial
        test_factor = gcd(abs((t-h)), trial)
        if isPrime(test_factor):
            factor = test_factor
    return factor

"""
-*- Baby Step Giant Step -*-
//inputs
p => mod (the mod num)
a => alpha (log base)
b => beta (num to find

//outputs
gives you x
"""
def baby_giant(p,a,b):
    def mod( a, b, answer):
        x = 0
        while x < b:
            modanswer = answer % b
            mod = (a * x) % b
            if mod == modanswer:
                print( "(" + str(a) + " * x) mod " + str(b) + " = " + str(answer))
                print("N = ", N)
                print( "a_inv = " + str(x))
                break
            x += 1
        return x

    N = math.floor((p-1)**.5) + 1

    a_inv = mod( a, p, 1)

    js = []
    ks = []

    j = 0
    k = 0
    print("A^j mod P    BA^(-NK) mod P")

    answerFound = False

    while answerFound == False:
        colm1 = (a ** j) % p
        colm2 = (b*(a_inv)**(N*k)) % p
        js.append(colm1)
        print(colm1, "       ", colm2)
        ks.append(colm2)
        for i in range(len(js)):
            if js[i] in ks:
                j_ans = i
                answerFound = True
        j += 1
        k += 1

    for i in range(len(ks)):
        if ks[i] == js[j_ans]:
            k_ans = i

    print("Answers, J = ", j_ans, "\t K = ", k_ans)

